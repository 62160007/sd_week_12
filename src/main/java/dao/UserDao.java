/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.*;
import java.sql.*;
import model.User;
import database.Database;

/**
 *
 * @author tatar
 */
public class UserDao implements DaoInterface<User> {

    @Override
    public int add(User object) {
        PreparedStatement stmt = null;
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO user (name,tel,password) VALUES (?,?,?)";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setString(3, object.getPassword());
            int row = stmt.executeUpdate();
            ResultSet rs = stmt.getGeneratedKeys();

            if (rs.next()) {
                id = rs.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("Can't open database! :(");
            System.exit(0);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<User> getAll() {
        ArrayList list = new ArrayList();
        Statement stmt = null;
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT * FROM user";
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String tel = rs.getString("tel");
                String password = rs.getString("password");
                User user = new User(id, name, tel, password);
                list.add(user);
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.out.println("Can't open database! :(");
            System.exit(0);
        }
        db.close();
        return list;
    }

    @Override
    public User get(int id) {
        ArrayList list = new ArrayList();
        Statement stmt = null;
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT * FROM user WHERE id = " + id;
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            if (rs.next()) {
                int uid = rs.getInt("id");
                String name = rs.getString("name");
                String tel = rs.getString("tel");
                String password = rs.getString("password");
                User user = new User(uid, name, tel, password);
                list.add(user);
                return user;
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.out.println("Can't open database! :(");
            System.exit(0);
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        PreparedStatement stmt = null;
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "DELETE FROM user WHERE id = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
            stmt.close();

        } catch (SQLException ex) {
            System.out.println("Can't open database! :(");
            System.exit(0);
        }
        db.close();
        return row;
    }

    @Override
    public int update(User object) {
        PreparedStatement stmt = null;
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE user SET name = ?,tel = ?,password = ?"
                    + " WHERE id = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setString(3, object.getPassword());
            stmt.setInt(4, object.getId());
            row = stmt.executeUpdate();
            System.out.println("Affect row : " + row);
            System.out.println("-----------------------");

            stmt.close();
        } catch (SQLException ex) {
            System.out.println("Can't open database! :(");
            System.exit(0);
        }
        db.close();
        return row;
    }

    public static void main(String[] args) {
        UserDao dao = new UserDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new User(-1, "Mio Sakamoto", "0547741254",
                "password"));
        System.out.println("id: " + id);
        User lastUser = dao.get(id);
        System.out.println("last user : " + lastUser);
        lastUser.setTel("0951111111");
        int row = dao.update(lastUser);
        User updateUser = dao.get(id);
        System.out.println("update user : " + updateUser);
        dao.delete(id);
        User deleteUser = dao.get(id);
        System.out.println("delete user : " + deleteUser);
    }
}
